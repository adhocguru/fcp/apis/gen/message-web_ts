// package: fcp.message_web.v1
// file: v1/message_web/message.proto

var v1_message_web_message_pb = require("../../v1/message_web/message_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var ChatService = (function () {
  function ChatService() {}
  ChatService.serviceName = "fcp.message_web.v1.ChatService";
  return ChatService;
}());

ChatService.GetMessages = {
  methodName: "GetMessages",
  service: ChatService,
  requestStream: false,
  responseStream: false,
  requestType: v1_message_web_message_pb.GetMessagesRequest,
  responseType: v1_message_web_message_pb.GetMessagesResponse
};

exports.ChatService = ChatService;

function ChatServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

ChatServiceClient.prototype.getMessages = function getMessages(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ChatService.GetMessages, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.ChatServiceClient = ChatServiceClient;

