// package: fcp.message_web.v1
// file: v1/message_web/message.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_message_web_common_common_pb from "../../v1/message_web_common/common_pb";

export class GetMessagesRequest extends jspb.Message {
  clearSortList(): void;
  getSortList(): Array<v1_message_web_common_common_pb.Sort>;
  setSortList(value: Array<v1_message_web_common_common_pb.Sort>): void;
  addSort(value?: v1_message_web_common_common_pb.Sort, index?: number): v1_message_web_common_common_pb.Sort;

  hasUserTo(): boolean;
  clearUserTo(): void;
  getUserTo(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserTo(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDelivery(): boolean;
  clearDelivery(): void;
  getDelivery(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDelivery(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasSendAt(): boolean;
  clearSendAt(): void;
  getSendAt(): v1_message_web_common_common_pb.TimeRange | undefined;
  setSendAt(value?: v1_message_web_common_common_pb.TimeRange): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMessagesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetMessagesRequest): GetMessagesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMessagesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMessagesRequest;
  static deserializeBinaryFromReader(message: GetMessagesRequest, reader: jspb.BinaryReader): GetMessagesRequest;
}

export namespace GetMessagesRequest {
  export type AsObject = {
    sortList: Array<v1_message_web_common_common_pb.Sort.AsObject>,
    userTo?: google_protobuf_wrappers_pb.StringValue.AsObject,
    delivery?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    sendAt?: v1_message_web_common_common_pb.TimeRange.AsObject,
  }
}

export class GetMessagesResponse extends jspb.Message {
  clearMessagesList(): void;
  getMessagesList(): Array<Message>;
  setMessagesList(value: Array<Message>): void;
  addMessages(value?: Message, index?: number): Message;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMessagesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetMessagesResponse): GetMessagesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMessagesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMessagesResponse;
  static deserializeBinaryFromReader(message: GetMessagesResponse, reader: jspb.BinaryReader): GetMessagesResponse;
}

export namespace GetMessagesResponse {
  export type AsObject = {
    messagesList: Array<Message.AsObject>,
  }
}

export class Message extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getMessageType(): number;
  setMessageType(value: number): void;

  hasSendAt(): boolean;
  clearSendAt(): void;
  getSendAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setSendAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getMessageBody(): string;
  setMessageBody(value: string): void;

  getUserFrom(): string;
  setUserFrom(value: string): void;

  getDelivery(): boolean;
  setDelivery(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Message.AsObject;
  static toObject(includeInstance: boolean, msg: Message): Message.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Message, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Message;
  static deserializeBinaryFromReader(message: Message, reader: jspb.BinaryReader): Message;
}

export namespace Message {
  export type AsObject = {
    id: number,
    messageType: number,
    sendAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    messageBody: string,
    userFrom: string,
    delivery: boolean,
  }
}

